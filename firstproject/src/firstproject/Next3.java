package firstproject;

public class Next3 {

	public static void main(String[] args) {
		int money = 1000;
		int product = 140;
		int change = money - product;

		System.out.println(product + "円の商品を" + money + "円で購入したおつりは…");


		if(change / 500 >= 1) {
			System.out.println("500円玉は" +(change / 500) + "枚");
			change = change % 500;
		}
		if(change / 100 >= 1) {
			System.out.println("100円玉は" +(change / 100) + "枚");
			change = change % 100;
		}
		if(change / 50 >= 1) {
			System.out.println("50円玉は" +(change / 50) + "枚");
			change = change % 50;
		}
		if(change / 10 >= 1) {
			System.out.println("10円玉は" +(change / 10) + "枚");
			change = change % 10;
		}

	}

}
