package firstproject;

public class Test2 {
	public static void main(String[] args) {
		int year = 2023;

		if(year % 4 == 0 && year % 100 == 0 && year % 400 != 0){
			System.out.println("うるうどしではない");
		}else if(year % 4 == 0) {
			System.out.println("年はうるう年");
		}else {
			System.out.println("うるうどしではない");
		}
	}

}
